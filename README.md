This simple Python script converts the output from a high performance code for 
density-functional theory calculations

Quantum Espresso http://www.quantum-espresso.org/ 

which contains calculated coordinates for an atomic structure and converts it 
into the common cif file format http://www.iucr.org/resources/cif . 
cif files can be visualized for instance with the free program 

VESTA http://jp-minerals.org/vesta/en/ . 

Sample input is given in sample_input.out and sample output is given in 
sample_output.cif. It is shown visualized in sample_visualized.PNG. 

More info on the scientific use can be found at https://arxiv.org/abs/1212.6782

Syntax: ./pw2cif.py input.out output.cif

Requires: Python and numpy. 