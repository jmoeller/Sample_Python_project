#! /usr/bin/python
# pwscf2cif
# Converts pwscf final coordinates after relax or vc-relax into a cif file
# Syntax: pwscf2cif input.out output.cif nscx nscy nscz
# nscx nscy nscz allow a supercell to be defined in the cif file (which has space group P1)
# Works for alat, crystal, bohr, and angstrom units with QE 4.3.2.
#
# Written by Johannes Moeller, Oxford Univ Dept of Physics
# j.moeller1@physics.ox.ac.uk
# Last change 13 October 2011: added supercell functionality
# Last change 20 Feb 2013: bugfix for vc-relax (not working correctly in versions < 2.0)

ver='2.0'

import sys, numpy, math, re
from numpy import *

# function to get cell parameters vectors from matched pattern
def get_cpv(cpb):
    v1=array([float(cpb.group(2)),float(cpb.group(3)),float(cpb.group(4))])
    v2=array([float(cpb.group(5)),float(cpb.group(6)),float(cpb.group(7))])
    v3=array([float(cpb.group(8)),float(cpb.group(9)),float(cpb.group(10))])
    return v1,v2,v3

# function to get metric tensor in (alat)^2 units from cell parameters    
def get_M(v1,v2,v3):
    M=array([[dot(v1,v1), dot(v1,v2), dot(v1,v3)],[dot(v2,v1), dot(v2,v2), dot(v2,v3)],[dot(v3,v1), dot(v3,v2), dot(v3,v3)]])
    return M
    
# function to get vector [v1.r v2.r v3.r] also in (alat)^2 units if input is
def get_abcr(v1,v2,v3,r):
    abcr=array([dot(v1,r), dot(v2,r), dot(v3,r)])
    return abcr

print 'pwscf2cif version', ver
print 'Syntax: input.out output.cif nscx nscy nscz (default: 1 for last three)'

# get command line arguments
infname=sys.argv[1]
ofname=sys.argv[2]
if len(sys.argv)>5:
    nscz=float(sys.argv[5])
else: 
    nscz=1.0
if len(sys.argv)>4:
    nscy=float(sys.argv[4])
else: 
    nscy=1.0
if len(sys.argv)>3:
    nscx=float(sys.argv[3])
else: 
    nscx=1.0

# checking in and output files
try:
    fin = open(infname, 'r')
except IOError:
    sys.stderr.write("ERROR opening or reading file %s\n" % (infname))
    sys.exit(1)

try:
    fout = open(ofname, 'w')
except IOError:
    sys.stderr.write("ERROR opening or writing file %s\n" % (ofname))
    sys.exit(1)    
    
inall=fin.read() # read complete file
fin.close()      

# find final coordinates block
flim=re.compile('Begin final coordinates.*End final coordinates',re.M|re.DOTALL)
if flim.search(inall)==None:
    sys.stderr.write("ERROR: could not find final coordinates block\n")
    sys.exit(1)
else:
    fcut=flim.search(inall).group() # get final coordinates block

# the block CELL_PARAMETERS 
# search pattern for block CELL_PARAMETERS 
cparslim=re.compile(r'CELL_PARAMETERS \(alat= ([0-9.]+)\).*\n[ ]*(-?[0-9.]+)[ ]*(-?[0-9.]+)[ ]*(-?[0-9.]+)[ ]*\n[ ]*(-?[0-9.]+)[ ]*(-?[0-9.]+)[ ]*(-?[0-9.]+)[ ]*\n[ ]*(-?[0-9.]+)[ ]*(-?[0-9.]+)[ ]*(-?[0-9.]+)[ ]*\n')
if cparslim.search(fcut)!=None:    
    cpb=cparslim.search(fcut)       # the block 
    alat=float(cpb.group(1))        # the alat parameter
else: # need CELL_PARAMTERS block from beginning of file
    alat=float(re.search(r'lattice parameter \(alat\)[ ]*=[ ]*(-?[0-9.]+)[ ]*a.u.',inall).group(1))
    # hack: use the same type of grouping as above, but now cparslim.search(inall).group(1) is not alat but just a(1)
    cparslim=re.compile(r'(a\(1\)) = \([ ]*(-?[0-9.]+)[ ]*(-?[0-9.]+)[ ]*(-?[0-9.]+)[ ]*\)[ ]*\n[ ]*a\(2\) = \([ ]*(-?[0-9.]+)[ ]*(-?[0-9.]+)[ ]*(-?[0-9.]+)[ ]*\)[ ]*\n[ ]*a\(3\) = \([ ]*(-?[0-9.]+)[ ]*(-?[0-9.]+)[ ]*(-?[0-9.]+)[ ]*\)[ ]*\n')
    cpb=cparslim.search(inall)       # the block 
    
v1,v2,v3=get_cpv(cpb) # get v1,v2,v3 cell parameters in alat units
M=get_M(v1,v2,v3)     # get metric tensor in alat units
    
# calcutlate lengths in angstrom
a=linalg.norm(v1)*alat*0.52917721
b=linalg.norm(v2)*alat*0.52917721
c=linalg.norm(v3)*alat*0.52917721

# calculate angles in degrees
aalpha=180/pi*arccos(dot(v2,v3)/(linalg.norm(v2)*linalg.norm(v3)))
abeta=180/pi*arccos(dot(v1,v3)/(linalg.norm(v1)*linalg.norm(v3)))
agamma=180/pi*arccos(dot(v1,v2)/(linalg.norm(v1)*linalg.norm(v2)))

#print aalpha, abeta, agamma, a, b, c

# get the units of ATOMIC_POSITIONS
if re.search('(ATOMIC_POSITIONS \()(.*)(\))',fcut)!=None:
    apunits=re.search('(ATOMIC_POSITIONS \()(.*)(\))',fcut).group(2) # the units of the atomic coords

# the block ATOMIC_POSITIONS: get atomic positions in whatever units they were in
aplim=re.compile(r'ATOMIC_POSITIONS \(.*\)\n(.*)\nEnd final coordinates',re.DOTALL)  # whole block
elposlim=re.compile('([a-zA-Z]+)\d*[ ]+(-?[0-9.]+)[ ]+(-?[0-9.]+)[ ]+(-?[0-9.]+)')  # elements and positions
if aplim.search(fcut)!=None:           # if there is such a block in fcut at all
    apall=aplim.search(fcut)           # the complete block ATOMIC_POSITIONS except header and footer
#    print apall.group()    
    elpos=elposlim.findall(apall.group(1)) # the list of all elements and their positions
    apos=zeros((len(elpos),3))    
    i=0
    for item in elpos: 
        apos[i][0]=float(item[1])
        apos[i][1]=float(item[2])
        apos[i][2]=float(item[3])                
#        print item[0], apos[i][0], apos[i][1], apos[i][2]
        i=i+1
else:
    sys.stderr.write("ERROR: could not find final ATOMIC_POSITIONS block\n")
    sys.exit(1)

# convert atomic positions into fractional coordinates 
fpos=zeros((len(elpos),3))
if apunits=='angstrom':
    i=0
    for item in apos[:]:
        abcr=get_abcr(v1,v2,v3,apos[i]/(0.52917721*alat)) # units are (alat)^2 since coordinates are converted from A into alat
        fpos[i]=linalg.solve(M,abcr)                      # solve M*fpos=apos[i] 
        i=i+1
elif apunits=='bohr':
    i=0
    for item in apos[:]:
        abcr=get_abcr(v1,v2,v3,apos[i]/(alat))    # units are (alat)^2 since coordinates are converted from bohr into alat
        fpos[i]=linalg.solve(M,abcr)              # solve M*fpos=apos[i] 
        i=i+1
elif apunits=='alat':
    i=0
    for item in apos[:]:
        abcr=get_abcr(v1,v2,v3,apos[i])           # units are (alat)^2 as apos[i] is already in alat units
        fpos[i]=linalg.solve(M,abcr)              # solve M*fpos=apos[i] 
        i=i+1
elif apunits=='crystal': # if units are crystal no action is required
    fpos=apos
else:
    sys.stderr.write("ERROR: check units of ATOMIC_POSITIONS\n")
    sys.exit(1)    
    
# create supercell (if desired)
natoms1=len(fpos)   # number of atoms in current supercell
nsc=nscx*nscy*nscz  # number of unit cells in supercell
fposnew=zeros((nsc*natoms1,3)) 

i=0     # atom number
scx=1
while scx <= nscx:
    scy=1
    while scy <= nscy:
        scz=1
        while scz <= nscz:
            for item in elpos:
                fposnew[i][0]=(fpos[i%natoms1][0]+(scx-1))/nscx # note the modulo operator n % m = remainder of n/m
                fposnew[i][1]=(fpos[i%natoms1][1]+(scy-1))/nscy
                fposnew[i][2]=(fpos[i%natoms1][2]+(scz-1))/nscz
                i=i+1
            scz=scz+1
        scy=scy+1
    scx=scx+1
fpos=fposnew

a=nscx*a
b=nscy*b
c=nscz*c


# output to cif file
fout.write('data_')
fout.write(infname)
fout.write('\n_audit_creating_method            \'generated by pwscf2cif version ')
fout.write(ver)
fout.write('\'\n')
fout.write('\n_cell_length_a                    ')
fout.write(str(a))
fout.write('\n_cell_length_b                    ')
fout.write(str(b))
fout.write('\n_cell_length_c                    ')
fout.write(str(c))
fout.write('\n_cell_angle_alpha                 ')
fout.write(str(aalpha)) 
fout.write('\n_cell_angle_beta                  ')
fout.write(str(abeta))
fout.write('\n_cell_angle_gamma                 ')
fout.write(str(agamma))
fout.write('\n_symmetry_space_group_name_H-M    \'P 1\'')
fout.write('\n_symmetry_Int_Tables_number        1\n\n')
fout.write('loop_\n_symmetry_equiv_pos_as_xyz\n   \'+x, +y, +z\'\n\n')
fout.write('loop_\n')
fout.write('   _atom_site_label\n')
fout.write('   _atom_site_type_symbol\n')
fout.write('   _atom_site_occupancy\n')
fout.write('   _atom_site_fract_x\n')
fout.write('   _atom_site_fract_y\n')
fout.write('   _atom_site_fract_z\n')

sc=1    # supercell number
i=0     # atom number
while sc<=nsc:
    for item in elpos:
        elid=''.join([item[0],str(i+1)])   # join element name with iterating number for unique id, separator is empty ''
        fout.write(elid.ljust(5))
        fout.write('  ')
        fout.write(item[0].ljust(3))
        fout.write('  1.0  ')
        fout.write(str(fpos[i][0]).ljust(18))
        fout.write('  ')
        fout.write(str(fpos[i][1]).ljust(18))
        fout.write('  ')
        fout.write(str(fpos[i][2]).ljust(18))
        fout.write('\n')
        i=i+1
    sc=sc+1
fout.close()

print 'Success! Output stored in', ofname

    
    
    









